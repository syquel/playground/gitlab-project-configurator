```{include} ../../changelog.md
:relative-docs: docs/
:relative-images:
```

First opensourced version

## History before opensourcing GPC

This is a quick summary of the history of the GPC releases before
opensourcing it.

### 1.0.0 Initial POC

- 3 execution modes (dry-run, apply, interactive)
- project selection using globing
- exclusion using regular expression
- custom rules
- Project settings supported:
  - secret variables
  - merge request settings
  - protected branches and tags
  - default branch and policy

### 1.0.1

- Mock init\_gitlab.
- Update min coverage to 88.
- Return an no 0 exit code if an error occurred.

### 2.0.0

- Important changes:
  - Every projects configured by GPC now have a GPC badge
  - `--diff` argument to display only changes
  - rules override keywords (`custom_rules` and `inherits_from`) now
    merge the new settings with the previous ones.
- New Features:
  - Policy inheritance (A depends on B)
  - New project settings supported:
    - add individual users to protected branches
    - Jira plugin configuration
    - Merge request approval
    - Enable/Disable Git LFS/Wiki/Snippets/Issues
    - Project badges

### 2.0.1

- Fix "rule override" in log ignoring custom rules.

### 2.0.2

- internal packaging changes

### 2.1.0

- New Features:
  - New project settings supported: Configure project schedulers

### 2.1.1

- Allow dot character in branch name.

### 2.1.2

- Add TU
- Don't update archived projects.

### 2.1.3

- Fix schedulers report.

### 2.1.4

- fix execution: `break` is not `continue` !!

### 2.1.5

- Use SortedList and dataclass classes.
- Refactoring of approval execution.
- Support file type for project variables and scheduler variables.

### 2.2.0

- New project settings supported:
  - Notify on changes
  - Support masked variables
  - Configure self aprroval

### 2.2.1

- Add pipelines-email settings.
- Fix bug in mail notifiction when change occured on jira section.
- Update python-gitlab to 1.10.0.

### 2.3.0

- New Features:
  - Simplify adding members to a project
  - Configure broken master mail notification
- Bug Fixes:
  - Handle properly projects that do not exist

### 2.3.1

- Masked regex: add @ and
- Display name of the variable on masked value issue

### 2.3.2

- Fix project already configured, if the project has an uppercase
  letter

### 2.4.0

- New Features:
  - Create keep parameter for
    - variables
    - protected branches
    - scheduled pipelines
    - protected tags
- Bug Fixes:
  - GPC does not configure shared project in a group.

### 2.4.1

- JIRA transition id is now optional

### 2.4.2

- Fix docker tag date computation.
- Fix jira property comparison.

### 2.4.3

- Add setting `remove_source_branch_after_merge`

### 2.4.4

- New Features
  - Jira integration
    - Add parameter `comment_on_event_enabled`
    - Add parameter `trigger_on_mr`
    - `jira_issue_transition_id` value can be an empty string (to
      remove the value), an integer, a list of integer (separated
      by `,` or `;`). This parameter is optional.

### 2.4.5

- Pay back techdeb on jira integration after Gitlab 12.7 deployment.
  <span class="title-ref">comment\_on\_event\_enabled</span> is now
  available in the Jira Service API
- Fix for pylint update.
- Workaround against yapf 0.27.
- Remove unnecessary list convertion.
- Fix `attr.s` deprecation warning.
- Update Python dependencies
- Update to python 3.7 by default

### 2.5.0

- New Feature:
  - Group configuration (variable only)
  - Add profiles for label. \[BISSERIE Vincent\]
- Bug fixes:
  - Fix issue during json generation. The value was not set in
    `FieldBean`.
  - Approval setting: fix incohrerent settings.
  - Update gpc to manage new endpoint approval rules.
  - Replace yapf by black for code formatting

### 2.5.1

- New Features
  - CI/CD Git Shallow Clone: `ci_git_shallow_clone`
  - Use `paths: /` to apply gpc on all gitlab projects.
- bug fix:
  - Fix email when we have a difference in project properties.
  - The execution is succesful, when all group and project
    configurations are applied successfully.
  - Raise an exception when all the approvers are not set, because,
    users are not members of project. \[guillaume.micouin-jorda\]

### 2.5.2

- Use cache in CI.

### 2.5.3

- Include the members of shared groups, during the check to add
  members on protected\_branches.
- Use cache property to store the members of project, as not to do the
  same request for each protected\_branch.
- CI: Publish stable and unstable image in same stage.

### 2.5.4

- New Features
  - Approvers: `enable_committers_approvers`
  - Add configuration of field `build_coverage_regex` and
        `auto_cancel_pending_pipelines`.
- bug fix:
  - Add missing owner, guest roles to project\_members.
  - Rename reporter role to reporters.
  - Add reporter role to project\_members.
  - Fix email notification, when a change exist on field
    `auto_cancel_pending_pipelines` or `build_coverage_regex`.
  - Use the name field for badge. Replace existing badge without the
    name field, by the same badge without the name field.
    (`name = link_url` by default)
  - Add name of badge in schema.

### 2.5.5

- New Features:
  - Configure group members.
- bug fix:
  - Add error messages in email notifications.
  - On a masked variable ending by `\n`, fix gpc that raises an
    error.

### 2.5.6

- Add property keep\_existing\_members.
