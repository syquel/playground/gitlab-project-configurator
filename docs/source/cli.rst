Command Line Interface
======================

This is the command line interface help.
The CLI might be useful for local tests and during development,
but the main usage of Gitlab-Project-Configurator is expected to be
in a Gitlab CI pipeline.

.. click:: gpc.cli:entrypoint
   :prog: gpc
   :show-nested:

'Defaults' file
---------------

GPC can take many parameters to configure its execution,
either as direct command line parameters (``--something``),
either as environment variable (``GPC_SOMETHING``) or
as 'default' settings in ``gpc.cfg`` file (``something = somevalue``).

The 'defaults' file can be configured by ``--defaults-file``
and is ``gpc.cfg`` by default.

Here the precedence on these ways to set the value of a parameter:

  - first the environment variable
  - then the command line interface
  - then the setting in the configuration file.

.. flat-table::
  :header-rows: 1
  :widths: 1 1 1

  * - Where
    - Format
    - Example

  * - Line in ``gpc.cfg``
    - ``something = value``
    - ``color = "force"``

  * - CLI parameter
    - ``--something``
    - ``--color=force``

  * - Environment variable
    - ``GPC_ + <uppercase>``
    - ``GPC_COLOR=force``

Here is an example of ``gpc.cfg`` file with default settings.

.. code-block:: ini

    # Configure the color output. Allowed values:
    #   no: disable color
    #   auto: find out if the CI is in a colorful terminal with a TTY
    #   force: force colors even if no TTY has been found
    color = "force"

    # Python-Gitlab configuration file
    gitlab_cfg = "~/.gitlab.cfg"

    # Default name of the JSON report file
    report_file = "report-gpc.json"
    # Default name of the HTML report file
    report_html = "report-gpc.html"

    # Default Gitlab Server URL
    gitlab_url = "https://gitlab.com"

    # Sentry DSN (Data Source Name)
    sentry_dsn = ""

    # Configure the GELF endpoint for detailed logs
    gelf_host = ""
    # Set the log level on the GELF output
    gelf_level = "DEBUG"

    # smtp server to send mail notification"
    smtp_server = ""
    # smtp port
    smtp_port = "25"

    # Set the URL of the 'Configured by GPC' badge
    configured_by_gpc_badge_url = "https://img.shields.io/badge/Configured%20by-GPC-green.svg"

    # List of Project's Badge image URL to keep on project
    accepted_external_badge_image_urls = [""]
