
Usage
=====

GPC is pretty easy to use. You just need to create a **private** GitLab
project you will configure a **scheduler** to apply rules defined in a
YAML (or JSON) file.

You (or your team) own this "configuration" project that uses GPC. Noone can
"see" your credentials or any of your secrets as long as you keep your
configuration project "private".

Here is how it looks in a nutshell:

.. figure:: images/gpc-workflow.png
  :alt: GPC Workflow

Projects that has been modified by GPC can appear automatically with a
"Project Badge" pointing to the configuration project:

.. figure:: images/gpc-badge.png
  :alt: GPC Workflow

There are two execution modes of GPC:

- ``dry-run``: execute this mode in a merge request on your project.
  GPC will perform as many test as possible with the projects without changing
  anything on them. For example this allows to fails before you merge a change
  in a masked variable that does not respect the Gitlab Masked variable Regular
  Expression.
- ``apply``: this is the main execution mode, you can do it on your ``master``.
  This mode will be as greedy as possible, applying as many change as possible
  and reporting all changes and failure after execution.

Prerequisites
~~~~~~~~~~~~~

1. Create a private project for your configuration. For example:
   **"myteamname-projects-config"**.
   You own this "config" project.
   Keep it as private as possible, since it may hold all the credentials you
   need to propagate to your projects.

   .. warning::

     Beware of the Gitlab membershipt hierarchy

     Remember the Gitlab Membership inheritance. Someone added in your
     top-most group will automatically be added in every projects beneath,
     **even for private projects**.

     You cannot "decrease" someone's role. See the `Permissions page on
     Gitlab's documentation <https://docs.gitlab.com/ee/user/permissions.html>`__
     for more information:


       If a user is both in a group’s project and the project itself, the
       highest permission level is used.

2. You need a Gitlab Token with "**owner**\ " privilege on the projects
   you want to administer.

   It is easier to put this user directly as owner of the group of your project.

3. email is mandatory

   Please note your user should have an email address linked to it or
   Gitlab may no accept its connection.

4. Get the Gitlab Access Token for this IPN and set in a project
   variable named: ``GPC_GITLAB_TOKEN``

   .. figure:: images/gpc-token.png
     :alt:

   .. important:: **Required Token Permission**

     The level of permission your token should have depends on the feature
     you wish to enable.

     By default, we recommend to use a dedicated token that is owner on all
     projects. But if you want to reuse a token of an existing IPN, its
     membership levels depends on the feature you wish to configure with GPC:


     .. flat-table::
       :header-rows: 1

       * - Feature
         - Minimum Permission

       * - General settings, merge requests
         - Maintainer

       * - Protected Branches and tags permission
         - Owner

5. Create the configuration files as defined in the next section

6. Once it works, define a Gitlab scheduler (twice a day is enough)

Recommended CI Integration
~~~~~~~~~~~~~~~~~~~~~~~~~~

First, you need to create the ``.gitlab-ci.yml`` file that is going to execute
GPC against your configuration files.

The proposed pipeline looks like this:

.. image:: images/gpc-pipeline.png

What it does:

- The ``test:config`` step does not change anything on the Gitlab server,
  but it displays what will be modified.

  Using the ``--diff`` parameter hides all unmodified projects.

  This step is to be executed inside a merge request in order to test your
  change before merge.

- The ``apply:config`` will execute the action on the Gitlab server.
  It is only executed on ``master``.

Here is an example of diff output (stdout) in dry run mode:

.. image:: images/gpc-dry-run-output.png

Here is our recommended ``.gitlab-ci.yaml``:

.. code-block:: yaml

    stages:
      - test
      - apply

    image: registry.gitlab.com/grouperenault/gitlab-project-configurator:latest
    # Note: you can use "stable" here to use a better validated version, but older.
    # Stable is updated at the end of each Epic.
    # "latest" (or "unstable") is the most up-to-date version

    variables:
      GPC_GITLAB_URL: https://gitlab.com  # Equivalent of --gitlab-url ...
      # There should be a GPC_GITLAB_TOKEN in your environment variables, equivalent of --gitlab-token ...
      GPC_REPORT_FILE: report-gpc.json # Equivalent of '--report-file report-gpc.json'
      GPC_REPORT_HTML: report-gpc.html  # Equivalent of '--report-html report-gpc.html'
                                        # (optional, this will generate an html report)
      GPC_CONFIG: my_config.yml  # equivalent of '--config my_config.yml'
      GPC_COLOR: force  # equivalent of '--color force'

    test:config:
      stage: test
      script:
        - gpc --diff
      artifacts:
        paths:
          - $GPC_REPORT_FILE
          - $GPC_REPORT_HTML
        expire_in: 30 days

    apply:config:
      stage: apply
      only:
        - master
        - schedules
        - triggers
      script:
        - gpc --mode apply
      artifacts:
        paths:
          - $GPC_REPORT_FILE
          - $GPC_REPORT_HTML
        expire_in: 30 days


- Ensure you have defined the ``GPC_GITLAB_TOKEN`` environment variable in your
  project settings.
  This is the Gitlab Private Access Token of a user that must have "owner" role
  to all the projects you wish to configure. Ideally put it as "owner" of the
  top-most group.

Email Notification
~~~~~~~~~~~~~~~~~~

You can be notified by email, when your GPC project make changes.

For that, on your GPC project, you need to define an environment variable
``GPC_WATCHERS``, and set email addresses separated by a semi-colon:
``foo@bar.com;bar@foo.com``.

The "watchers" will receive an email titled "A change has been made by gpc"

.. image:: images/gpc-email-report.png

You can also, define the list of email addresses as argument of gpc script:

.. code-block:: bash

    $ gpc -m apply -c my_config.yml --watchers foo@bar.com;bar@foo.com

Report files
~~~~~~~~~~~~

Two report files can be generated if you want to keep track of what GPC did.

use the ``--report-file report.json`` and ``--report-html report.html``
parameters:

- the JSON file is a structured summary of every changes and errors

  .. image:: images/gpc-json-report.png

- the HTML report is a nicer representation of this JSON report

  .. image:: images/gpc-html-report.png

Preview mode
~~~~~~~~~~

GPC provides an opt-in option to enable a new behavior on configuration.
This is meant to become a standard option in an upcoming releases,
but this is kept disabled by default to avoid breaking existing configuration.

You can enable preview mode by defining the environment variable "GPC_PREVIEW=1"
or the ``--preview`` command line parameter.

The preview mode only changes for the moment the way list are merged when rules
are inherited.
As of today, when a rule inherits from another one, dictionaries are merged but
lists are replaced. This mimicks the way "Gitlab-CI" merges dictionaries
(variables, tags,...) and lists ("scripts", "rules",...).

Preview mode will now "extend" list instead of replacing the content.
This will allow to define default values in the base rules and all inherited
rules will automatically get them.

This comes at a cost (thus the "breaking change"): the inherited rule cannot
have less values in a list (cannot remove existing items).

Preview mode disabled (default):

.. code-block:: yaml

  projects_rules:
  - rule_name: root_policy
    project_members:
      members:
        - some.name

  projects_rules:
    - rule_name: derived_policy
      inherits_from: root_policy
      project_members:
        members:
          - some.name  # need to recopy
                      # since lists are
                      # replaced
          - another.person


Preview mode enabled (``GPC_PREVIEW=1``):

.. code-block:: yaml

  projects_rules:
    - rule_name: root_policy
      project_members:
        members:
          - some.name

  projects_rules:
    - rule_name: derived_policy
      inherits_from: root_policy
      project_members:
        members:
          - another.person
          # some.name is automatically added
